import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Home',
      url: '/folder/Home',
      icon: 'Home'
    },
    {
      title: 'Profile',
      url: '/profile',
      icon: 'people'
    },
    {
      title: 'Cash flow',
      url: '/cashflow',
      icon: 'cash'
    },
    {
      title: 'Net worth',
      url: '/networth',
      icon: 'car'
    },
    
    {
      title: 'Goals',
      url: '/goals',
      icon: 'golf'
    },
    {
      title: 'Retirement',
      url: '/retirement',
      icon: 'flower'
    }
   
  ];
  // public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
     
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }
}
