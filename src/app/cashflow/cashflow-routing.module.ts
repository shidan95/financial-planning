import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CashflowPage } from './cashflow.page';

const routes: Routes = [
  {
    path: '',
    component: CashflowPage
  },
  {
    path: 'income',
    loadChildren: () => import('./income/income.module').then( m => m.IncomePageModule)
  },
  {
    path: 'expenses',
    loadChildren: () => import('./expenses/expenses.module').then( m => m.ExpensesPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CashflowPageRoutingModule {}
