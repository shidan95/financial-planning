import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CashflowPage } from './cashflow.page';

describe('CashflowPage', () => {
  let component: CashflowPage;
  let fixture: ComponentFixture<CashflowPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashflowPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CashflowPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
