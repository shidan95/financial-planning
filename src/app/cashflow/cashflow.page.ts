import { Component, OnInit } from '@angular/core';
import {NavController, ModalController} from '@ionic/angular';
import { IncomePage } from '../cashflow/income/income.page';
import { ExpensesPage } from '../cashflow/expenses/expenses.page';
import { ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-cashflow',
  templateUrl: './cashflow.page.html',
  styleUrls: ['./cashflow.page.scss'],
})
export class CashflowPage implements OnInit {
  expenseList: Array<any>;
  
  constructor(
    public activatedRoute: ActivatedRoute,
    public nav:NavController,
    public mc:ModalController,
    public storage: Storage
    ) { 
      this.expenseList = [
        {
          name: "Baju",
          amt: "200"
        }
    ];
    
    for (let expense of this.expenseList){
      console.log(expense.name, expense.amt);

    }
  }

  ngOnInit() {
    let expenseData: any;
    this.storage.get('EXPENSELIST').then(response => {
      expenseData = response;
      if(expenseData){
        this.expenseList = JSON.parse(expenseData);
      }
    }) 
  }

  async openIncome(){
    let myMC =  await this.mc.create({
      component:IncomePage
    })
     return await myMC.present();
  }

  async openExpenses(){

    let myMC =  await this.mc.create({
      component:ExpensesPage
    })

    myMC.onDidDismiss().then(response => {
      console.log('data received: ', response.data);
      if (response.data != null){
        this.expenseList.push(response.data);
        this.storage.set('EXPENSELIST', JSON.stringify(this.expenseList));
      }
    });

     return await myMC.present();
  }

 


}
