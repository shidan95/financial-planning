import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';


@Component({
  selector: 'app-expenses',
  templateUrl: './expenses.page.html',
  styleUrls: ['./expenses.page.scss'],
})
export class ExpensesPage implements OnInit {
  expense: any;

  constructor(
    public mc:ModalController
  ) {
  this.expense = {
    name: null,
    amt: null
   };
  }

  saveExpense(){
    this.mc.dismiss(this.expense);
  }
 
  ngOnInit() {
  }

  closeModal ()
  {
    this.mc.dismiss()
  }


}
