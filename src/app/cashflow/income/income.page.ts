import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-income',
  templateUrl: './income.page.html',
  styleUrls: ['./income.page.scss'],
})
export class IncomePage implements OnInit {

  constructor(
    private mc:ModalController
    ) { }

  closeModal ()
  {
    this.mc.dismiss()
  }

  ngOnInit() {
  }

}
