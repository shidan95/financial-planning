import { Component, OnInit } from '@angular/core';
import {NavController, ModalController} from '@ionic/angular';

@Component({
  selector: 'app-goals',
  templateUrl: './goals.page.html',
  styleUrls: ['./goals.page.scss'],
})
export class GoalsPage implements OnInit {

  constructor(
    public modalController: ModalController,
    private nav:NavController,
  ) { }
  


  ngOnInit() {
  }

}
