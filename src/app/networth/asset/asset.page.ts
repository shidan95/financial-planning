import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
@Component({
  selector: 'app-asset',
  templateUrl: './asset.page.html',
  styleUrls: ['./asset.page.scss'],
})
export class AssetPage implements OnInit {

  constructor(
    private mc:ModalController
  ) { }

  closeModal()
  {
    this.mc.dismiss()
  }
  
  ngOnInit() {
  }

}
