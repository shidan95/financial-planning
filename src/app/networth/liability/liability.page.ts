import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
@Component({
  selector: 'app-liability',
  templateUrl: './liability.page.html',
  styleUrls: ['./liability.page.scss'],
})
export class LiabilityPage implements OnInit {

  constructor(
    private mc:ModalController
  ) { }

  closeModal()
  {
    this.mc.dismiss()
  }

  ngOnInit() {
  }

}
