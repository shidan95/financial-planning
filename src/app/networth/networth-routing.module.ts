import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NetworthPage } from './networth.page';

const routes: Routes = [
  {
    path: '',
    component: NetworthPage
  },
  {
    path: 'asset',
    loadChildren: () => import('./asset/asset.module').then( m => m.AssetPageModule)
  },
  {
    path: 'liability',
    loadChildren: () => import('./liability/liability.module').then( m => m.LiabilityPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NetworthPageRoutingModule {}
