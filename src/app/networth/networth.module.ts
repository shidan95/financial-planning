import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NetworthPageRoutingModule } from './networth-routing.module';

import { NetworthPage } from './networth.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NetworthPageRoutingModule
  ],
  declarations: [NetworthPage]
})
export class NetworthPageModule {}
