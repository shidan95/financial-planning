import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NetworthPage } from './networth.page';

describe('NetworthPage', () => {
  let component: NetworthPage;
  let fixture: ComponentFixture<NetworthPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NetworthPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NetworthPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
