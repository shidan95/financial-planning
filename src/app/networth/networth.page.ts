import { Component, OnInit } from '@angular/core';
import {NavController,ModalController} from '@ionic/angular';
import {AssetPage} from '../networth/asset/asset.page';
import{ LiabilityPage } from '../networth/liability/liability.page'
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-networth',
  templateUrl: './networth.page.html',
  styleUrls: ['./networth.page.scss'],
})
export class NetworthPage implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    private nav:NavController,
    private mc:ModalController
  ) { }

  public cash:string
  public namaArray:any=[];

  masukDalamArray(){
    this.namaArray.push(this.cash);
    console.log(this.namaArray)
  }

  async openAsset()
{
  let myMC=await this.mc.create({
    component:AssetPage
  })
  return await myMC.present();
}

async openLiability()
{
  let myMC=await this.mc.create({
    component:LiabilityPage
  })
  return await myMC.present();
}
  ngOnInit() {
  }

}
