import { Component, OnInit } from '@angular/core';
import{NavController,ModalController} from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  constructor(
    private nav:NavController,
    private modalCtrl:ModalController
  ) { }

  public nama:string
  public namaArray:any =[];
  public logo:any;

  masukDalamArray(){
    this.namaArray.push(this.nama);
    console.log(this.namaArray)
    this.logo=this.nama
  }
  
  ngOnInit() {
  }

}
