import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RetirementPage } from './retirement.page';

const routes: Routes = [
  {
    path: '',
    component: RetirementPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RetirementPageRoutingModule {}
