import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RetirementPageRoutingModule } from './retirement-routing.module';

import { RetirementPage } from './retirement.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RetirementPageRoutingModule
  ],
  declarations: [RetirementPage]
})
export class RetirementPageModule {}
