import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RetirementPage } from './retirement.page';

describe('RetirementPage', () => {
  let component: RetirementPage;
  let fixture: ComponentFixture<RetirementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetirementPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RetirementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
